//acceptable heresy :)
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unused_parens)]

use std::time;

const VERSION: &str = env!("CARGO_PKG_VERSION");
static mut CONFIG: Config = Config::default();

fn config() -> &'static mut Config { //this was neceasary :/
	unsafe{&mut CONFIG}
}

fn main() {
	let batteryManager = match battery::Manager::new() {
		Ok(m) => m,
		Err(e) => {println!("{e}"); notify(&format!("could initialize batteryManager\nerror:{}",e)); panic!();}
	};
	
	let mut battery: Option<battery::Battery> = None;
	
	//features
	#[allow(unused_assignments)] #[allow(unused_mut)]
	let mut lenovoConservationMode: bool = false;
	
	//get the battery
	for (i, tmp_battery) in match batteryManager.batteries() {
		Ok(b) => b,
		Err(e) => {println!("{e}"); notify(&format!("couldn't get battery\nerror:{}",e)); panic!();}
	}.enumerate(){
		if i > 0 {notify(&format!("more than one battery not supported as of {}", VERSION)); panic!("more than one battery not supported");}
		battery = match tmp_battery {
			Ok(b) => Some(b),
			Err(e) => {println!("{e}"); notify(&format!("couldn't get battery\n {}",e)); panic!();}
		};
	}
	
	let mut battery = battery.unwrap(); //i win "`battery` used here but it is possibly-uninitialized"
	
	loadConfig();
	
	#[cfg(feature = "lenovoConservationMode")]
	match std::fs::read("/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode") {
		Ok(v) => match String::from_utf8_lossy(&v).to_string().chars().nth(0) {
			Some(c) => if c == '1' { lenovoConservationMode = true; } else { lenovoConservationMode = false; },
			None => lenovoConservationMode = false
		},
		Err(..) => lenovoConservationMode = false
	};
	
	//variables
	let mut notifyUnknown = true;
	let mut notifyCharging = true;
	let mut notifyDischarging = true;
	let mut notifyFull = true;
	let mut notifyLow = true;
	
	#[cfg(feature = "lenovoConservationMode")]
	let mut notifyLenovoConservationMode = true;
	
	
	loop {
		match batteryManager.refresh(&mut battery) {
			Ok(..) => {},
			Err(e) => {println!("{e}"); notify("couldn't refresh battery\n {e}"); panic!();}
		};
		
		use battery::State;
		match battery.state() {
			State::Unknown => {
				if config().notifyUnknown && notifyUnknown && !lenovoConservationMode {
					notify("unknown state");
					notifyUnknown = false;
				}
				// if conservationmode is active probably this is it
				#[cfg(feature = "lenovoConservationMode")]
				if lenovoConservationMode && notifyLenovoConservationMode && config().notifyFull {
					notify("charge complete\nlenovo conservation mode");
					notifyLenovoConservationMode = false;
				}
			},
			State::Charging => {
				if config().notifyCharging && notifyCharging {
					notify("charging");
					notifyCharging = false;
				}
				notifyLow = true;
				notifyDischarging = true;
				notifyUnknown = true;
				notifyFull = true;
			},
			State::Discharging => {
				if config().notifyDischarging && notifyDischarging {
					notify("discharging");
					notifyDischarging = false;
					notifyCharging = true;
					#[cfg(feature = "lenovoConservationMode")]
					{ notifyLenovoConservationMode = true; }
				}
				use battery::units::ratio::percent;
				if config().notifyLow && notifyLow && (battery.state_of_charge().get::<percent>() < config().batteryLow){
					notify("low battery");
					notifyLow = false;
					notifyCharging = true;
				}
			},
			State::Empty => {/* ??? */},
			State::Full => {
				if config().notifyFull && notifyFull {
					notify("completly charged");
					notifyFull = false;
				}
				notifyLow = true;
				notifyDischarging = true;
				notifyUnknown = true;
			},
			_ => {},
		};
		std::thread::sleep(time::Duration::from_millis(config().batteryUpdateTime));
	}
}

struct Config {
	notifyUnknown: bool,
	notifyCharging: bool,
	notifyDischarging: bool,
	notifyFull: bool,
	notifyLow: bool,
	notifyTimeout: u32,
	batteryLow: f32,
	batteryUpdateTime: u64
}

impl Config {
fn to_string(&self) -> String {
	["notifyUnkown:", &self.notifyUnknown.to_string(), "\n",
	"notifyCharging:", &self.notifyCharging.to_string(), "\n",
	"notifyFull:", &self.notifyFull.to_string(), "\n",
	"notifyLow:", &self.notifyLow.to_string(), "\n",
	"notifyTimeout:", &self.notifyTimeout.to_string(), "\n",
	"batteryLow:", &self.batteryLow.to_string(), "\n",
	"batteryUpdateTime:", &self.batteryUpdateTime.to_string(), "\n",
	].concat()
}

const fn default() -> Self { //can't implement trait for this one :/
	Self { //default config
		notifyUnknown: true,
		notifyCharging: false,
		notifyDischarging: true,
		notifyFull: false,
		notifyLow: true,
		notifyTimeout: 10000, //milis, 0 for no timeout
		batteryLow: 20., //battery percentage for the low notification
		batteryUpdateTime: 5000
	}
}

fn reset(&mut self) {
	*self = Self::default();
}
}

use notify_rust::Notification;
use notify_rust::Timeout;
use notify_rust::Urgency;

fn notify(msg: &str) {
	Notification::new()
	.summary("Battery")
	.body(msg)
	.timeout(Timeout::Milliseconds(config().notifyTimeout))
	.urgency(Urgency::Normal)
	.show()
	.expect("notification fails");
}

use std::fs::File;
use std::io::Write;
use std::io::Read;

fn loadConfig() {
	let mut configPath = dirs::config_dir().unwrap();
	configPath.push("lexbatterynotify");
	
	let mut file = match File::open(configPath.clone()) {
		Ok(f) => f,
		Err(..) => {
			match File::create(configPath.clone()) {
				Ok(mut f) => {notify(&*("created config file at: ".to_owned()+configPath.to_str().unwrap())); match f.write(config().to_string().as_bytes()) {
					Ok(..) => {},
					Err(..) => {notify(&*("couldn't write to config file at: ".to_owned()+configPath.to_str().unwrap()+".\nusing defaults")); return ()}
				}; return ();}, //FIXME
				Err(..) => {notify("can't create/find config file.\nusing defaults"); return ()}
			}
		}
	};
	let mut content = String::new();
	match file.read_to_string(&mut content) {
		Ok(..) => {},
		Err(..) => {notify("can't read/invalid config file.\nusing defaults"); return();}
	}
	let mut content = content.lines();
	'parsing: loop {
		let line = match content.next() {
			Some(d) => d,
			None => break 'parsing
		};
		let data: Vec<&str> = line.split(':').collect();
		if data.len() != 2 {notify(&("invalid config at: ".to_owned()+line+".\nusing defaults")); return ()}
		match data[0] {
			"notifyUnkown" => {config().notifyUnknown = match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"notifyUnkown\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"notifyCharging" => {config().notifyCharging = match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"notifyCharging\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"notifyFull" => {config().notifyFull= match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"notifyFull\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"notifyLow" => {config().notifyLow= match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"notifyLow\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"notifyTimeout" => {config().notifyTimeout= match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"notifyTimeout\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"batteryLow" => {config().batteryLow = match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"batteryLow\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			"batteryUpdateTime" => {config().batteryUpdateTime= match data[1].parse() {
				Ok(v) => v,
				Err(..) => {notify("config \"batteryUpdateTime\" invalid.\nusing defaults"); config().reset(); return ();}
			}},
			_=>{}
		}
	}
}